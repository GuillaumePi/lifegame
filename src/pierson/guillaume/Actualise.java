package pierson.guillaume;

import javax.swing.*;

class Actualise extends Thread
{
    JFrame frame;
    Actualise(JFrame frame)
    {
        this.frame=frame;
        start();
    }
    public void run()
    {
        try
        {
            frame.invalidate();
            frame.repaint();
            sleep(750);
        }
        catch(Exception ignored)
        {
        }
    }
}
