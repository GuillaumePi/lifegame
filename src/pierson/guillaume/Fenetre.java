package pierson.guillaume;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Fenetre extends JFrame {
    private final int ROW_MAX = 50;
    private final int COLUMN_MAX = 50;

    private int generateCount = 0;

    private JPanel panelAction = new JPanel(new FlowLayout(FlowLayout.LEFT, 50, 5));
    private JPanel panelTableau = new JPanel();

    private JLabel lblGenerateCount = new JLabel(String.valueOf(generateCount));
    private JLabel lblNbGeneration = new JLabel("Nombre de génération : ");

    private JTextField txtNbGeneration = new JTextField(5);

    private JButton cmdStart = new JButton("Start");
    private JButton cmdStop = new JButton("Stop");

    private MouseAdapter mouseAdapter = new MouseAdapter() {
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
                System.out.println("Double click sur la cellule x = " + tblPetri.getSelectedColumn() + " et y = " + tblPetri.getSelectedRow());
                if (tblPetri.getValueAt(tblPetri.getSelectedRow(), tblPetri.getSelectedColumn()) == null) {
                    tblPetri.setValueAt(".", tblPetri.getSelectedRow(), tblPetri.getSelectedColumn());
                } else if (tblPetri.getValueAt(tblPetri.getSelectedRow(), tblPetri.getSelectedColumn()) == ".") {
                    tblPetri.setValueAt(null, tblPetri.getSelectedRow(), tblPetri.getSelectedColumn());
                }
            }
        }
    };

    private DefaultTableModel tblPetriModel = new DefaultTableModel(0, COLUMN_MAX) {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    private Object[][] tblPetriTableau = new Object[COLUMN_MAX][ROW_MAX];

    private JTable tblPetri = new JTable();

    private int nbGeneration;

    public Fenetre() {
        this.setSize(800, 883);
        this.setTitle("Life Game");

        ImageIcon icone = new ImageIcon("imgLifeGame.png");
        this.setIconImage(icone.getImage());

        this.setResizable(false);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getWidth() / 2, dim.height / 2 - this.getHeight() / 2);

        init();

    }

    private void init() {

        Border borderAction = BorderFactory.createTitledBorder("Action");
        panelAction.setBorder(borderAction);

        panelAction.add(lblNbGeneration);
        panelAction.add(txtNbGeneration);
        panelAction.add(cmdStart);

        panelAction.add(lblGenerateCount);



        cmdStart.addActionListener(e -> {
            if (txtNbGeneration.getText().isEmpty() || txtNbGeneration.getText() == null || txtNbGeneration.getText().equals("0")) {
                nbGeneration = 1;
            } else {
                nbGeneration = Integer.parseInt(txtNbGeneration.getText());
            }

            SwingWorker<Void, Void> worker = new SwingWorker<>() {
                @Override
                protected Void doInBackground() {
                    for (int i = 0; i < nbGeneration; i++) {
                        generate();
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    }
                    return null;
                }

            };
            worker.execute();

        });


        Border borderTableau = BorderFactory.createTitledBorder("Petri's Box");
        panelTableau.setBorder(borderTableau);

        for (int i = 0; i < ROW_MAX; i++) {
            tblPetriModel.addRow(tblPetriTableau[i]);
        }

        tblPetri.setModel(tblPetriModel);

        tblPetri.setCellSelectionEnabled(true);
        tblPetri.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblPetri.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < COLUMN_MAX; i++) {
            tblPetri.getColumnModel().getColumn(i).setPreferredWidth(15);

        }
        tblPetri.setRowHeight(15);

        tblPetri.setDefaultRenderer(Object.class, new MonJTableRender());

        tblPetri.addMouseListener(mouseAdapter);

        /**
         * Figures initiales
         */
        tblPetri.setValueAt(".", 5, 5);
        tblPetri.setValueAt(".", 5, 6);
        tblPetri.setValueAt(".", 5, 7);

        tblPetri.setValueAt(".", 5, 15);
        tblPetri.setValueAt(".", 6, 15);
        tblPetri.setValueAt(".", 7, 15);
        tblPetri.setValueAt(".", 7, 14);
        tblPetri.setValueAt(".", 6, 13);

        panelTableau.add(tblPetri, BorderLayout.CENTER);

        Container contentPane = this.getContentPane();
        contentPane.add(panelAction, BorderLayout.NORTH);
        contentPane.add(panelTableau, BorderLayout.CENTER);
    }

    public synchronized void generate() {
        int count = 0;
        Object[][] tblPetriTableauNext = new Object[COLUMN_MAX][ROW_MAX];

            for (int i = 0; i < ROW_MAX; i++) {
                for (int j = 0; j < COLUMN_MAX; j++) {
                    tblPetriTableau[i][j] = tblPetri.getValueAt(j, i);
                }
            }

            for (int i = 1; i < ROW_MAX - 1; i++) {
                for (int j = 1; j < COLUMN_MAX - 1; j++) {
                    count = look(j, i);

                    if (tblPetriTableau[i][j] == ".") {
                        if (count >= 4 || count < 2) {
                            tblPetriTableauNext[j][i] = null;
                        } else
                        {
                            tblPetriTableauNext[j][i] = ".";
                        }
                    } else if (tblPetriTableau[i][j] == null)
                        {
                        if (count == 3) {
                            tblPetriTableauNext[j][i] = ".";
                        }
                    }
                }
            }

            tblPetriTableau = new Object[ROW_MAX][COLUMN_MAX];
            tblPetriTableau = tblPetriTableauNext;
            DefaultTableModel tableModelEmpty = (DefaultTableModel) tblPetri.getModel();
            tableModelEmpty.setRowCount(0);

            for (int i = 0; i < ROW_MAX; i++) {
                tblPetriModel.addRow(tblPetriTableau[i]);
            }

            tblPetri.setModel(tblPetriModel);

        generateCount++;
        lblGenerateCount.setText(String.valueOf(generateCount));


}

    public int look(int row, int column){
        int nbCellActiv = 0;

        if (tblPetriTableau[column + 1][row] == ".")
            nbCellActiv++;
        if (tblPetriTableau[column - 1][row] == ".")
            nbCellActiv++;
        if (tblPetriTableau[column][row + 1] == ".")
            nbCellActiv++;
        if (tblPetriTableau[column][row - 1] == ".")
            nbCellActiv++;
        if (tblPetriTableau[column + 1][row + 1] == ".")
            nbCellActiv++;
        if (tblPetriTableau[column + 1][row - 1] == ".")
            nbCellActiv++;
        if (tblPetriTableau[column - 1][row + 1] == ".")
            nbCellActiv++;
        if (tblPetriTableau[column - 1][row - 1] == ".")
            nbCellActiv++;

        return nbCellActiv;
    }

}
