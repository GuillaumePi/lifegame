package pierson.guillaume;

import javax.swing.*;

public class Application {

    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e){ }
        new Application();
    }

    public Application(){
        Fenetre fenetre = new Fenetre();
        fenetre.setVisible(true);
    }
}
