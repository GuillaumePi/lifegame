package pierson.guillaume;

import java.awt.*;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class MonJTableRender extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);


            JLabel label = (JLabel) component;
            if (label.getText().equals(".")) {
                component.setBackground(Color.BLACK);
            }else{
                component.setBackground(Color.WHITE);
            }


        return component;
    }
}
